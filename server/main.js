import { Meteor } from 'meteor/meteor';

import '../imports/api/publications/bluetoothDevices'
import '../imports/api/methods/bluetooth'


Meteor.startup(() => {
  console.log('Server started'); // traza manu
  // TODO: crear rutina que analice los puertos del sistema y cree un arbol en base a los dispositivos almacenados en BD
  const exec = require('child_process').exec;
  const bindedPorts = [];
  exec('rfcomm',
    (error, stdout, stderr) => {
      if (error) console.log('exec error: ', error);
      if (stderr) console.log('stderr: ', stderr);

      console.log(stdout);
      const portStrings = stdout.split('\n');
      console.log(portStrings);
      if (!portStrings.length) {
        console.log('Ningún puerto en el sistema operativo'); // traza manu
        return;
      }

      // FIXME: falla el exec en la segunda fila; me devuelve null y falla el mapeo
      // const regEx = /(\S+):\s(.*->\s)*((\d{2}(:|.)){6}).*(closed|connected)/ig;
      // const regEx = /(\S+):.*/ig;
      const regEx = /(rfcomm\d*).*(\d{2}:\d{2}:\d{2}:\d{2}:\d{2}:\d{2})\schannel.*/ig;

      portStrings.map((portString) => {
        // console.log(portString); // traza manu
        const portItems = regEx.exec(portString);
        console.log(portItems); // traza manu
        return portItems;
        console.log({
          sistemPort: portItems[1],
          macAddress: portItems[3],
          status: portItems[6],
        }); // traza manu
        bindedPorts.push();
      });
      return;
    });
});
