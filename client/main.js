import { Meteor } from 'meteor/meteor';

// ojo con la versión del build de vue: https://vuejs.org/v2/guide/installation.html#Explanation-of-Different-Builds
import Vue from 'vue'; // Sólo runtime, sin compilador
// import Vue from 'vue/dist/vue.common';

// plugin para integración vue+meteor
import VueMeteorTracker from 'vue-meteor-tracker';
Vue.use(VueMeteorTracker);


// Componentes
import App from '../imports/ui/App.vue';

Meteor.startup(() => {
  // Si meto el compilador no funciona; parece que se carga la propiedad "meteor" que defino en el componente, porque no no veo por ningún sitio
  // new Vue({
  //   el: '#app',
  //   template: '<app/>', // TODO: no entiendo muy bien este parámetro
  //   components: { App },
  // });

  new Vue({
    render: h => h(App),
  }).$mount('#app');
});
