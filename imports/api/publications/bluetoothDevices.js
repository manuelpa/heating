import { Meteor } from 'meteor/meteor';

import { BtDevices } from '../collections/bluetoothDevices';

Meteor.publish('bluetoothDevices', () => {
    console.log('Estoy publicando BtDevices'); // traza manu
  return BtDevices.find();
});
