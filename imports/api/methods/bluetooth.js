import { Meteor } from 'meteor/meteor'
import { check } from 'meteor/check'
import { BtDevices } from '../collections/bluetoothDevices'
import { Titles } from '../collections/titles'
import five from 'johnny-five'
import SerialPort from 'serialport'

const instancesContainer = {};

const btDiscoverableSerialPort = new (require('bluetooth-serial-port')).BluetoothSerialPort();
function deviceToDatabase(payload) {
  console.log('Insertando en BD'); // traza manu
  BtDevices.insert(payload);
}
function handleDeviceFoundEvent(addressFound, deviceName) {
  console.log('Encontrado dispositivo: ', deviceName, addressFound); // traza manu
  // if (BtDevices.find({ address: { $eq: addressFound } }).count()) {
  //   console.log('Obviando, dispositivo conocido'); // traza manu
  //   return;
  // }

  console.log(`Scanning channels on ${deviceName}...`); // traza manu
  btDiscoverableSerialPort.findSerialPortChannel(addressFound,
    Meteor.bindEnvironment(function handleChannelFoundEvent(channelFound) {
      console.log('Encontrado canal ', channelFound, 'en ', deviceName); // traza manu

      deviceToDatabase({
        address: addressFound,
        name: deviceName,
        channel: channelFound,
      });
    }),
    function handleChannelFoundError() {
      console.log('Canal no encontrado en ', deviceName);
  });
}
Meteor.methods({
  'scanForDevices'() {
    console.log('Escaneando...'); // traza manu

    /* Grupo par pruebas => leyendo puertos del systema */
    if (false) {
      var SerialPort = require('serialport');
      SerialPort.list((error, ports) => {
        if (error) console.log(error);
        ports.forEach(function(port) {
          console.log(port.comName);
          console.log(port.pnpId);
          console.log(port.manufacturer);
          console.log(port);
        });
      })
      throw new Meteor.Error('pants-not-found', "Can't find my pants");
    }


    btDiscoverableSerialPort.on('finished', () => { console.log('Finalizado el escaneo'); });
    // https://guide.meteor.com/using-npm-packages.html#bind-environment
    btDiscoverableSerialPort.on('found', Meteor.bindEnvironment(handleDeviceFoundEvent));
    btDiscoverableSerialPort.inquire();

    return 'Escaneando espectro Bluetooth...';
  },
  'connectBoard'(address) {
    const device = BtDevices.findOne({ address: address });
    // cosa harto improbable pero ...
    if (!device) throw new Meteor.Error('device-not-found', "Dispositivo no encontrado");

    // TODO: Antes de establecer la conexión comprobar si existe la instancia => desactivar
    // ¿eliminando la instancia? No es buena idea dejar la electrónica activada y sin control.
    // Pero tengo que encontrar la manera de desactivar Bluetooth para ahoorar en consumo de baterías 
    if (instancesContainer[device.serialPort]) {
      console.log('ya existe'); // traza manu
      delete instancesContainer[device.serialPort];
      console.log(instancesContainer); // traza manu
      return 'disconnected ';
    }

    // Creo una nueva instancia
    console.log('nueva instancia'); // traza manu
    instancesContainer[device.serialPort] = new five.Board({
      port: `/dev/${device.serialPort}`,
      // REPL: hay que desactivarlo si pretendemos usar jhonny-five desde init.d
      // https://github.com/rwaldron/johnny-five/wiki/Board#sub-process-usage
      repl: false,
      // timeout: 24000,
    });

    // Eventos de placa
    // http://johnny-five.io/api/board/#events
    instancesContainer[device.serialPort].on('message', function(event) {
      console.log('mensaje: ', event); // traza manu
    });
    instancesContainer[device.serialPort].on("exit", function() {
      console.log('exit success'); // traza manu
      this.digitalWrite(13, 0);
    });
    instancesContainer[device.serialPort].on('ready', function() {
      console.log('ready'); // traza manu
      const boardInstance = this;

      // led de control => "ON"
      this.pinMode(13, five.Pin.OUTPUT);
      this.digitalWrite(13, 1);

      // enciendo el sensor levantando el pin 9
      /*
      this.pinMode(9, five.Pin.OUTPUT);
      this.digitalWrite(9, 1);

      let temperature = new five.Thermometer({
        controller: "LM35",
        pin: "A0",
        freq: 2000,
      });

      // desconecta BT tras 25 lecturas
      let readings = 0;
      temperature.on("data", function() {
        if (readings++ >= 25) {
          boardInstance.digitalWrite(9, 0);
        }
        console.log("lectura: ", readings);
        console.log("celsius: %d", this.C);
        // console.log("fahrenheit: %d", this.F);
        // console.log("kelvin: %d", this.K);
      });
      */



      /*
      this.pinMode(0, five.Pin.ANALOG);
      this.analogRead(0, function(voltage) {
        console.log(voltage);
      });
      */

      this.loop(2000, function() {
        console.log('connected: ', boardInstance.isConnected);
      });
    });

    return device.serialPort;
  },
  'updateDevice'(address, port) {
    check(address, String);
    check(port, String);

    const device = BtDevices.findOne({ address });
    if (!device) throw new Meteor.Error('device-not-found', "Dispositivo no encontrado");

    BtDevices.update({ _id: device._id }, { $set: { serialPort: port } });
  },
});
